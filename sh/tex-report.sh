#!/bin/sh

# $1 - project name

mkdir -p $1

cd ./$1
cp -n ~/Templates/XeTeX/* ./
mv ./Report.tex ./$1.tex
mv ./auto/Report.el ./auto/$1.el
sed -i "1iNAME := $1" Makefile #prepend NAME variable to makefile
