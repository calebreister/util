#!/bin/sh
# Simple script to open a Windows URL shortcut in Firefox

url=$(grep URL=* $1 | head -1)
firefox --new-tab ${url#URL=}
