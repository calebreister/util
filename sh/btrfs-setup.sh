#!/bin/sh

# Create and mount btrfs subvolumes for system installation
# $1: partition to configure

# Debug mode
# set to "echo" for debugging
dbg=""

# Subvolume locations and names
vol_dirs="home \
     	  var/cache/pacman/pkg \
	 	  var/abs \
		  var/tmp \
		  srv"
vol_names=${vol_dirs//\//-}

# Convert to arrays and generate indices
vol_dirs=( $vol_dirs )
vol_names=( $vol_names )
idx=$(seq 0 $[${#vol_dirs[*]}-1])

# Create root subvolume
$dbg mount $1 /mnt
$dbg btrfs sub create /mnt/@

# Create subdirectories and subvolumes
for i in $idx
do
	$dbg mkdir -p /mnt/@/${vol_dirs[i]}
	$dbg btrfs sub create /mnt/@${vol_names[i]}
done

# Unmount
$dbg umount /mnt

# Remount for system installation
$dbg mount $1 -t btrfs -o subvol=@ /mnt

for i in $idx
do
	$dbg mount $1 -t btrfs -o subvol=@${vol_names[i]} /mnt/${vol_dirs[i]} 
done
