#!/bin/sh

# Copyright (c) 2017 Caleb Reister
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# UUID of the encrypted backup partition
UUID=db0bdf15-31ec-4626-adb9-3a686899b534

comm_next_ver() {
    # Determines the newest common version from two file lists (using ls -1)
    # $1: "source" directory
	# $2: "destination" directory

	# List files
	local src=$(mktemp)
	local dst=$(mktemp)
	local com=$(mktemp)
	ls -1 $1 > $src
	ls -1 $2 > $dst

	# Get newest common and oldest new version numbers
	sort -n $src $dst | uniq -d | tee $com | tail -n 1
	sort -n $src $com | uniq -u | head -n 1

    # Clean up
    rm $src $dst $com
}

next_ver() {
	# Determines the oldest snapshot not on the backup drive
    # $1: "source" directory
	# $2: "destination" directory

	local src=$(mktemp)
	local dst=$(mktemp)
	ls -1 $1 > $src
	ls -1 $2 > $dst

    sort -n $src $dst | uniq -d | sort -n $src - | uniq -u | head -n 1

    rm $src $dst
}

if [ "$(whoami)" != "root" ]; then
    echo Please run this script as root.
    exit
fi

# Mount drives
if mountpoint -q "/mnt/bu"; then
	echo '/mnt/bu already mounted.'
else
	cryptsetup open UUID="$UUID" bu --key-file /key/${UUID}.key
	mount -o compress=lzo /dev/mapper/bu /mnt/bu
fi

# Locate snapshot directory
config=$1
prefix=$(snapper list-configs | grep $config | sed "s/ //g" | awk -F'|' '{print $2;}' | sed s'/\/$//')/.snapshots

# Determine newest common version
src=$(mktemp)
dst=$(mktemp)
com=$(mktemp)
ls -1 $prefix > $src
ls -1 /mnt/bu/$config > $dst

# Get newest common and oldest new version numbers
old=$(sort -n $src $dst | uniq -d | tee $com | tail -n 1)
new=$(sort -n $src $com | uniq -u | head -n 1)

rm $src $dst $com

#old=$(echo $ver | head -n 1)
#new=$(echo $ver | tail -n 1)
#old=$(comm_ver $prefix /mnt/bu/$config)
#new=$(next_ver $prefix /mnt/bu/$config)

# Confirm before sending
read -p "Snapshot $new (parent $old) will be sent to /mnt/bu/$config. Continue [y/N]? " send
if [ "$send" = "${send#[Yy]}" ]; then
	exit
fi

# Send snapshots and related info to external drive
mkdir /mnt/bu/$config/$new
cp $prefix/$new/info.xml /mnt/bu/$config/$new
btrfs send -p $prefix/$old/snapshot $prefix/$new/snapshot | pv -btr | btrfs receive /mnt/bu/$config/$new
sync

# Clean up & unmount backup drive
read -p 'Unmount/lock backup drive [y/N]? ' unmount
if [ "$unmount" != "${unmount#[Yy]}" ]; then
	umount /mnt/bu && echo '/mnt/bu unmounted'
	cryptsetup luksClose bu
fi
