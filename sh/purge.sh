#!/bin/sh

# Copyright (c) 2017 Caleb Reister
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Purge a drive by filling it with random data. This script uses a special trick
# to make the process as fast as possible: instead of writing data from
# /dev/urandom directly, it generates a AES key and uses it to encrypt /dev/zero
# so that hardware acceleration can be used.
#
# WARNING! This script is irreversible and dangerous. Use it with extreme caution.
#
# $1: block device to erase

echo "Are you sure you want to overwrite $1 with random data?"
read -p "Confirm the device path: " idiot_check

if [ $idiot_check = $1 ]; then
   # Get size of block device (used by pv to show progress)
   SIZE=$(blockdev --getsize64 $1)

   # Generate random data and pipe it to the drive
   openssl enc -pbkdf2 -pass \
		   pass:"$(dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64)" \
		   -nosalt </dev/zero \
	   | pv -bartpes $SIZE \
	   | dd bs=64K of=$1
else
	echo 'Idiot check failed.'
fi
