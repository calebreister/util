# Maintainer: Alex Dewar <alex.dewar@gmx.co.uk>
# Contributor: Ray Rashif <schiv@archlinux.org>
# Contributor: Tobias Powalowski <tpowa@archlinux.org>

pkgname=opencv-cuda
pkgver=4.0.1
pkgrel=2
provides=(opencv opencv-samples)
conflicts=(opencv opencv-samples)
pkgdesc="Open Source Computer Vision Library with CUDA support"
arch=(x86_64)
license=(BSD)
url="http://opencv.org/"
options=(staticlibs)
depends=(intel-tbb openexr gst-plugins-base libdc1394 cblas lapack libgphoto2 jasper cuda)
makedepends=(cmake python-numpy python2-numpy mesa eigen hdf5 lapacke qt5-base nvidia-sdk)
optdepends=('opencv-samples: samples'
            'qt5-base: highgui support'
            'hdf5: support for HDF5 format'
            'opencl-icd-loader: For coding with OpenCL'
            'python-numpy: Python 3 interface'
            'python2-numpy: Python 2 interface')
source=("opencv-$pkgver.tar.gz::https://github.com/opencv/opencv/archive/$pkgver.zip"
        "opencv_contrib-$pkgver.tar.gz::https://github.com/opencv/opencv_contrib/archive/$pkgver.tar.gz"
        "fix-cuda-10.1.patch")
sha256sums=('b79ccdc4797a959c5ab17249a8a302c066248ae070e4d7010e2d77a625fdb30a'
            '0d8acbad4b7074cfaafd906a7419c23629179d5e98894714402090b192ef8237'
            '4e1640f37ee357d38551a65d3dbfc03a5d7589bffaa3cde92a64c5ea62f55aef')

prepare() {
  msg2 "Patching sources for CUDA v10"
  sed -i 's|nvcuvid.h|nvidia-sdk/nvcuvid.h|' opencv_contrib-$pkgver/modules/cud*/src/*.hpp

  mkdir -p build

  cd opencv-$pkgver
  patch --forward --strip=1 < ../fix-cuda-10.1.patch
}

build() {
  cd build
  # cmake's FindLAPACK doesn't add cblas to LAPACK_LIBRARIES, so we need to specify them manually
  _pythonpath=`python -c "from sysconfig import get_path; print(get_path('platlib'))"`
  cmake ../opencv-$pkgver \
    -D WITH_OPENCL=ON \
    -D WITH_OPENGL=ON \
    -D WITH_TBB=ON \
    -D OpenGL_GL_PREFERENCE=GLVND \
    -D BUILD_WITH_DEBUG_INFO=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_PERF_TESTS=OFF \
    -D BUILD_EXAMPLES=ON \
    -D INSTALL_C_EXAMPLES=ON \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D CMAKE_INSTALL_LIBDIR=lib \
    -D CPU_BASELINE_DISABLE=SSE3 \
    -D CPU_BASELINE_REQUIRE=SSE2 \
    -D WITH_NVCUVID=ON \
    -D WITH_CUDA=ON \
    -D WITH_CUBLAS=ON \
    -D CUDA_HOST_COMPILER=/usr/bin/gcc-7 \
    -D OPENCV_EXTRA_MODULES_PATH="$srcdir/opencv_contrib-$pkgver/modules" \
    -D OPENCV_SKIP_PYTHON_LOADER=ON \
    -D OPENCV_PYTHON3_INSTALL_PATH=$_pythonpath \
    -D LAPACK_LIBRARIES="/usr/lib/liblapack.so;/usr/lib/libblas.so;/usr/lib/libcblas.so" \
    -D LAPACK_CBLAS_H="/usr/include/cblas.h" \
    -D LAPACK_LAPACKE_H="/usr/include/lapacke.h" \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
	-D ENABLE_FAST_MATH=ON \
    -D CUDA_FAST_MATH=ON \
	-D WITH_GSTREAMER=ON \
	-D WITH_GSTREAMER_0_10=OFF \
	-D WITH_LIBV4L=ON \
	-D WITH_QT=ON 
  make -j4
}

package() {
  cd build
  make DESTDIR="$pkgdir" install

  # install license file
  install -Dm644 "$srcdir"/opencv-$pkgver/LICENSE -t "$pkgdir"/usr/share/licenses/$pkgname

  cd "$pkgdir"/usr/share

  # separate samples package; also be -R friendly
  if [[ -d opencv4/samples ]]; then
    mv opencv4 $pkgname # otherwise folder naming is inconsistent
  elif [[ ! -d opencv4 ]]; then
    warning "Directory naming issue; samples package may not be built!"
  fi
}
