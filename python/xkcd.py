#!/usr/bin/python
# Download xkcd comics by their index numbers (passed as arguments)
import sys
import json
import urllib.request
import posixpath

# Configuration
basePath = '/home/caleb/Pictures/Comics/xkcd/'

def getComic(num):
    # Download alt text
    url = urllib.request.urlopen('http://xkcd.com/'+str(num)+'/info.0.json')
    data = json.loads(url.read().decode('utf-8')) # comic metadata
    altText.append(str(num)+': '+data['alt'])
    # Download image
    #name = re.search('(?<=\/)[\w]+\.[\w]{3}', data['img'])
    name = posixpath.basename(data['img'])
    img = urllib.request.urlretrieve(data['img'], basePath+str(num)+'-'+name)

# Read alt text file into memory
altFile = open(basePath+'0-alt.txt', encoding='utf-8')
altText = [line.strip() for line in altFile]
altFile.close()

# Process user input and download comic(s)
sys.argv.pop(0) # remove argv[0] (name of script)
if len(sys.argv) == 0: # Download most recent comic if sys.argv is empty
    url = urllib.request.urlopen('http://xkcd.com/info.0.json')
    num = json.loads(url.read().decode('utf-8'))['num']
    sys.argv.append(num)
for comicNum in sys.argv: # Download comic(s)
    # Skip duplicates
    for l in altText:
        if (str(comicNum)+':' in l):
            continue
    getComic(comicNum)

# Write alt text file to disk
altFile = open(basePath+'0-alt.txt', 'w', encoding='utf-8')
for l in altText:
    if l == '': continue
    altFile.write(l+'\n\n')
altFile.close()
