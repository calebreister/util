-- GrayUpCounter_sim.vhdl
--
-- Testbench for GrayUpCounter
--
-- Copyright 2018 Caleb Reister
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity GrayUpCounter_sim is end;

architecture sim of GrayUpCounter_sim is
    signal clk   : std_logic := '1';
    signal reset : std_logic := '1';
    signal count : unsigned(3 downto 0);
    signal gray  : std_logic_vector(3 downto 0);
    signal bin : std_logic_vector(3 downto 0);

begin
    CLOCK : process
    begin
        wait for 5ns;
        clk <= not clk;
    end process;
    
    TestDev : entity work.GrayUpCounter
        generic map(
            SIZE => 4
        )
        port map(
            clk    => clk,
            reset  => reset,
            enable => '1',
            count  => count,
            gray   => gray
        );

    G2B : entity work.GrayToBinary
        generic map(
            SIZE => 4
        )
        port map(
            gray => gray,
            bin => bin
        );

    TEST_DATA : process
    begin
        wait for 40ns;
        reset <= '0';
        wait;
    end process;
end architecture;
