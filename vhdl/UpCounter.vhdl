-- UpCounter.vhdl
-- Implements a generic up counter with synchronous or asynchronous reset and
-- clock enable.
-- 
-- Copyright 2018 Caleb Reister
-- 
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
-- 
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
-- 
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity UpCounter is
    generic(
        SIZE       : positive  := 16;    -- Number of bits in the counter
        SYNC_RESET : boolean   := true;  -- Synchronous reset if true
        RESET_POL  : std_logic := '1'    -- Reset polarity ('1' => active-high)
    );
    port(
        clk    : in  std_logic;
        reset  : in  std_logic;
        enable : in  std_logic;
        count  : out unsigned(SIZE-1 downto 0)
    );
end entity;

architecture synth of UpCounter is
    -- Internal count signal
    signal c : unsigned(SIZE-1 downto 0) := (others => '0');

begin
    -- Attach output to internal counter signal
    count <= c;

    -- Synchronous reset
    SYNC : if SYNC_RESET = true generate
        COUNTER : process(clk)
        begin
            if rising_edge(clk) then
                if (reset = RESET_POL) then
                    c <= (others => '0');
                elsif (enable = '1') then
                    c <= c + 1;
                end if;
            end if;
        end process;
    end generate;

    -- Asynchronous reset
    ASYNC : if SYNC_RESET = false generate
        COUNTER : process(clk, reset)
        begin
            if (reset = RESET_POL) then
                c <= (others => '0');
            elsif rising_edge(clk) then
                if (enable = '1') then
                    c <= c + 1;
                end if;
            end if;
        end process;
    end generate;
end architecture;
