#!/bin/sh
# dedup.sh
# 
# Copyright 2018 Caleb Reister
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

help() {
	echo '
dedup.sh [-h] [-l] [-a algorithm] [-i ignore ...] [-c default-command]
         [-r read-log] [find-path ...]

This script uses checksums to find and interactively remove duplicate files
using a simple command syntax. Although it is not strictly POSIX-compliant,
it should work on most Unix-like systems.

    -h             Display this help message and exit.

    -l             Copies the list of potential duplicates and their checksums
                   into the working directory before exiting.
   
    -a algorithm   Specifies the hashing algorithm utility to use for file 
                   comparison, such as cksum (default), md5sum, or sha1sum. Note
                   that in some instances md5sum may be faster than cksum.

    -i ignore      Ignore files and directories with the given name; this is
                   useful when searching for duplicates in directories that may
                   contain code repositories or local configuration files. This
                   flag may be given multiple times.
                   Example: dedup.sh -i .git -i .gitignore

    -c command     Specifies a command to apply for every set of duplicate
                   files. See below for a list of the supported commands.
                   For example, `dedup.sh -c ^1` deletes all but the first of
                   every duplicate.

    -r read-log    Reads a tab-delimited list of checksums and filenames and
                   locates duplicates.

After the options are processed, all remaining arguments are passed to the find
command as starting directories.     

--------------------------------------------------------------------------------

Duplicate file prompt commands:

[1] ./duplicate1
[2] ./duplicate2
[3] ./duplicate3
[4] ./duplicate4
[5] ./duplicate5
> ^
> *
> 4
> 2-4
> ^1

Command           | Description
------------------|-----------------------------
`^` or `K` or `k` | Keep all
`*`               | Delete all
`i`               | Delete the file at index *i*
`i-j` or `i:j`    | Delete files between indices *i* and *j*
`^i`              | Keep only file *i*

Note: multiple index and range commands (such as `4` or `2-4` above) can be
used multiple times on a single prompt if they are delimited using spaces or
commas. For instance, `1 3-5` or `1,3-5` is equivalent to `^2`.
'
}

# Delete temporary files and exit the script (with error code $1)
cleanup() {
	echo
	echo 'Cleaning...'
	if [ "$LOG" = '1' ]; then
		cp $CKSUMS ./$(basename $CKSUMS).log
		echo "Log written to ./$(basename $CKSUMS).log"
	fi
	rm -f $PREFIX $SIZES $ALL_FILES $FILES $CKSUMS $DELETE
	exit $1
}

tempfile() {
	# $1: name for temporary file
	# $2: number of bits to use for random identifier
	echo $1.$(od -An -N$2 -tx1 /dev/urandom | tr -d ' ')
}

# Trap early termination conditions in order to clean temporary files
trap 'cleanup 2' ABRT
trap 'cleanup 1' QUIT
trap 'cleanup 1' TERM
trap 'cleanup 1' INT

# Create temporary files
export TMPDIR=${TMPDIR:-/tmp} # Define TMPDIR, if necessary

PREFIX=$(tempfile $TMPDIR/dedup.prefix 4) # List of search directories
SIZES=$(tempfile $TMPDIR/dedup.sizes 4) # List of file sizes
ALL_FILES=$(tempfile $TMPDIR/dedup.all_files 4) # List of all files
FILES=$(tempfile $TMPDIR/dedup.files 4) # List of possible duplicate files
CKSUMS=$(tempfile $TMPDIR/dedup.cksums 4) # cksum/hash output
DELETE=$(tempfile $TMPDIR/dedup.delete 4) # List of files to delete

touch $PREFIX $SIZES $ALL_FILES $FILES $CKSUMS $DELETE

# Process arguments
while getopts ':i:a:c:r:lh' opt; do
	case $opt in
		i)
			# Append to list of folders to ignore
			IGNORE="${IGNORE} ! -path */${OPTARG}/* ! -name */${OPTARG}"
			;;
		a)
			# Choose algorithm
			HASH=$OPTARG
			;;
		c)
			CMD=$OPTARG
			;;
		r)
			READ=$OPTARG
			;;
		l)
			LOG=1
			;;
		h)
			help
			exit
			;;
		*)
			echo "Invalid option: $opt"
			help
			cleanup 1
			;;
	esac
done
shift $((OPTIND-1))

while [ "$1" != '' ]; do
	if [ -d "$1" ]; then
		# Append to PREFIX if argument is a valid path
		echo $1 >> $PREFIX
	else
		echo "Invalid argument: $1"
		help
		cleanup 1
	fi
	shift
done

# Set default values for unassigned parameters
HASH=${HASH:-cksum}
CMD=${CMD:-0}
if [ "$(wc -l $PREFIX | cut -d' ' -f1)" = "0" ]; then
	echo ./ > $PREFIX
fi

if [ "$READ" = '' ]; then
	# Generate file list, ignore files with a unique size.
	# 
	# Note: du does not do an exact byte count, so some files will be processed
	# unnecessarily.

	while read -r dir; do
		find "$dir" -type f $IGNORE -exec du {} + | sort -k 1,1 | tee $ALL_FILES \
			| cut -f1 | uniq -d > $SIZES
		join -t "$(echo '\t')" -1 1 -2 1 $SIZES $ALL_FILES | cut -f2 >> $FILES
	done < $PREFIX

	echo "Found $(wc -l $FILES | cut -d' ' -f1) potential duplicates. Running ${HASH}..."

	# Compute checksums and sort the output by size
	# 
	# Note: the -0 option of xargs is not in the POSIX specification, but seems to
	# be one of the only ways for it to support spaces and special characters.
	cat $FILES | tr '\n' '\0' | xargs -P8 -0 $HASH | cut -d' ' -f1-2 \
		| paste - $FILES > $CKSUMS
else
	cp $READ $CKSUM
fi
# Find duplicate checksums and populate the list of files to delete
awk -v DELETE=$DELETE -v CMD=$CMD -F'\t' -f ~/util/dedup/dedup.awk $CKSUMS

if [ "$?" = '0' ]; then
	# Delete files
	echo 'The following files have been marked for deletion...'
	cat $DELETE
	read -p 'Are you sure you want to delete these files [y/N]? ' delete
	if [ "$delete" != "${delete#[Yy]}" ]; then
		while read -r file; do
			rm "$file"
			echo Delete $file
		done < $DELETE
	fi
fi

# Clean up temporary files
cleanup 0
