# dedup.awk
# 
# Copyright 2018 Caleb Reister
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# This awk script is meant to accompany dedup.sh. It searches for duplicate
# files based on columns output by the cksum utility and asks the user which
# files to delete.

{
	# dup[size,cksum,index]: potential duplicates organized by size and checksum
	# count[size,cksum]: number of duplicates for a particular size and checksum
	# first[size,cksum]: first occurrence of a filename with a particular size
	#     and checksum
	
	if (count[$1] == 1) {
		dup[$1,count[$1]] = first[$1];
		count[$1]++;
	}
	if (count[$1] > 1) {
		dup[$1,count[$1]] = $2;
		count[$1]++;
	}
	else {
		first[$1] = $2;
		count[$1] = 1;
	}
}

END {
	for (cksum in count) {
		count[cksum]--;
		if (count[cksum] == 0)
	        delete count[cksum];
	}

	s = sum(count);
	if (s == 0) {
		print "No duplicates found. Exit.";
		exit 1;
	}
	
	print "Found " sum(count) " duplicates...";
	
	# Iterate over duplicate file groups (files with the same size and checksum)
	for (cksum in count) {
		# Prompt user for files to keep
		if (CMD == 0) {
			print ""
			for (n = 1; n <= count[cksum]; n++)
				print "[" n "] " dup[cksum,n];
		}
		
		do {
			invalid = 0;
			
			# Prompt for list of files to delete
			if (CMD == 0) {
				do {
					printf "> ";
					getline prompt < "-"
				} while (prompt == "" || prompt ~ /[ \t\n]+/);
			}
			else
				prompt = CMD;
			
			# Parse prompt
			split(prompt, del_p, "[ ,]");
			
			# Generate list of files to delete
			if (del_p[1] == "*") {
				# Delete all
				print "Delete all";
				for (n = 1; n <= count[cksum]; n++)
					del[cksum,n] = dup[cksum,n];
			}
			else if (del_p[1] ~ /^[Kk\^]$/) {
				print "Keep all";
			}
			else if (del_p[1] ~ /^[\^Kk]([1-9][0-9]*)$/) {
				# Keep one item
				k = substr(del_p[1], 2);
				if ((cksum,k) in dup) {
					print "Keep " dup[cksum,k];
					for (n = 1; n <= count[cksum]; n++)
						if (n != k) del[cksum,n] = dup[cksum,n];
				}
				else {
					print "Invalid keep index: " del_p[1];
					invalid = 1;
				}
			}
			else {
				for (p in del_p) {
					if (del_p[p] ~ /^[1-9][0-9]*$/) {
						# Delete index
						n = del_p[p] + 0;
						if ((cksum,n) in dup)
							del[cksum,n] = dup[cksum,n];
						else {
							print "Invalid index: " del_p[p];
							invalid = 1;
						}
					}
					else if (del_p[p] ~ /^([1-9][0-9]*)[-:]([1-9][0-9]*)$/) {
						# Delete range
						split(del_p[p], r, "[-:]");
						if (((cksum,r[1]) in dup) &&
							((cksum,r[2]) in dup))
						{
							seq(range, r[1], r[2]);
							for (n in range)
								del[cksum,n] = dup[cksum,n];
						}
						else {
							print "Invalid range: " del_p[p];
							invalid = 1;
						}
					}
					else {
						print "Invalid command: " del_p[p];
						invalid = 1;
					}
				}
			}
			
			# Invalid input
			if (invalid) {
				# Clear the relevant portion of the delete list
				for (n = 1; n <= count[cksum]; n++)
					delete del[cksum,n];
				if (CMD == 0) {
					print "Exit. Nothing will be deleted."
					exit 2;
				}
			}
		} while (invalid)
	}
	
	# Write list of files to delete
	for (d in del)
		print del[d] >> DELETE;
}

# l = seq(dest, a, b)
#
# Generates an array containing integers between two values, and returns the
# length of the array. The order of the a and b arguments determines the order
# in which values are generated (incrementing or decrementing).
#
# Arguments:
# - dest: destination array into which the sequence is written
# - a: first value in range
# - b: last value in range
#
# Return: length of dest
#
# Local variables: _i, _count
function seq(dest, a, b, _i, _count) {
	# Clear contents of dest array
	for (_i in dest)
		delete dest[_i];

	# Generate sequence
	_count = 0;
	if (a <= b) {
		for (_i = a; _i <= b; _i++) {
			dest[_count++] = _i;
		}
	}
	else {
		for (_i = a; _i >= b; _i--) {
			dest[_count++] = _i;
		}
	}
	return _count;
}

function sum(array, _i, _sum) {
	_sum = 0;
	for (_i in array)
		_sum += array[_i];
	return _sum;
}

function asize(array, _i, _count) {
	_count = 0;
	for (_i in array)
		_count++;
	return _count;
}

function aprint(array, _i, _i_str) {
	for (_i in array) {
		_i_str = _i;
		sub(SUBSEP, "," _i_str);
		print "[" _i_str "]:", array[_i];
	}
}
